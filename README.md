# Requirements

You can deploy this proxy using maven, a pom.xml file is provided in the root directory.

Node.js and NPM (Node package manager) need to be installed to be able to deploy the API proxy and run the integration tests using cucumber-js.

# API proxy deployment

	$ mvn install -Dapigee.username=USERNAME -Dapigee.password=PASSWORD -Dapigee.org=ORGANIZATION -Dapigee.env=ENVIRONMENT

# API proxy testing

Whenever you deploy the API proxy, some integration tests will be run. If you would like to just run the tests without 
	
	$ mvn install -Dapigee.org=ORGANIZATION -Dapigee.env=ENVIRONMENT -DskipDeployment
