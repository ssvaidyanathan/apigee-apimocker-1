Feature: As a user I would like my mock to mock anything

    Scenario: Hello World

        When I GET /helloworld
        Then response code should be 200
        And response body path $.message should be Hello world

    Scenario: Good Morning Miren

        When I GET /goodmorning?name=Miren
        Then response code should be 200
        And response body path $.message should be Buenos días Miren

    Scenario: Good Morning Sean

        When I GET /goodmorning?name=Sean
        Then response code should be 200
        And response body path $.message should be Good morning Sean

    Scenario: Good Morning Ozan

        When I GET /goodmorning?name=Ozan
        Then response code should be 200
        And response body path $.message should be Günaydın Ozan

    Scenario: Good Morning Nicola

        When I GET /goodmorning?name=Nicola
        Then response code should be 200
        And response body path $.message should be Buon giorno Nicola

    Scenario: Good Morning Yuriy

        When I GET /goodmorning?name=Yuriy
        Then response code should be 200
        And response body path $.message should be Доброго ранку Юрій
